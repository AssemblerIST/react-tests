import React from 'react';
import ReactDOM from 'react-dom';



//const store = createStore(reducer, composeWithDevTools(applyNiddleware(thunk)));


import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(
  <div>
    <App />
  </div>,
  document.getElementById('root'));
registerServiceWorker();
